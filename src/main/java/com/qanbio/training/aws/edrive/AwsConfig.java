package com.qanbio.training.aws.edrive;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

public interface AwsConfig {
    String ACCESS_KEY_PROP = "aws.s3.access_key";
    String SECRET_KEY_PROP = "aws.s3.secret_Key";
    String BUCKET_NAME_PROP = "aws.s3.bucket_name";
    String REGION_PROP = "aws.region";

    default String getAccessKey() {
        return getProperty(ACCESS_KEY_PROP);
    }

    default String getSecretKey() {
        return getProperty(SECRET_KEY_PROP);
    }

    default String getBucketName() {
        return getProperty(BUCKET_NAME_PROP);
    }

    default String getRegion() {
        return getProperty(REGION_PROP);
    }

    default String getProperty(String key) {
        return PropertiesLoader.getInstance().getProperty(key);
    }

    default AmazonDynamoDB buildDynamoDbClient() {
        return AmazonDynamoDBClientBuilder.standard()
                .withRegion(Regions.fromName(getRegion()))
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(getAccessKey(), getSecretKey())))
                .build();
    }

    default AmazonS3 buildS3Client() {
        return AmazonS3ClientBuilder.standard()
                .withRegion(Regions.fromName(getRegion()))
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(getAccessKey(), getSecretKey())))
                .build();
    }
}
