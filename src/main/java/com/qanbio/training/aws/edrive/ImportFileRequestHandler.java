package com.qanbio.training.aws.edrive;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Import a given file into Amazon S3
 */
public class ImportFileRequestHandler implements RequestHandler<Bill, Bill>, AwsConfig {
    private static String CONTENT_TYPE = "application/json";

    public Bill handleRequest(Bill bill, Context context) {
        LambdaLogger logger = context.getLogger();

        String billAsJson = mapToJson(bill, logger);

        //=== Build metadata
        S3FileMetadata metadata = S3FileMetadata.builder()
                .contentType(CONTENT_TYPE)
                .bucketName(getBucketName())
                .fileName(bill.getBillId())
                .build();

        //=== Build S3 Client
        AmazonS3 s3Client = buildS3Client();

        //=== Build DynamoDB Client
        AmazonDynamoDB dbClient = buildDynamoDbClient();
        DynamoDBMapper dbMapper = new DynamoDBMapper(dbClient);

        //=== Save Object into Dynamo and S3
        try {
            s3Client.putObject(metadata.getBucketName(), bill.getBillId(), billAsJson);
            dbMapper.save(bill);
        } catch (AmazonServiceException ex) {//S3 internal error
            logger.log("S3 internal error, ErrorCode=" + ex.getErrorCode() + ",  ErrorMessage=" + ex.getErrorMessage());
            throw ex;
        } catch (SdkClientException ex) {//Unable to contact S3
            logger.log("Unable to contact S3" + ex.getMessage());
            throw ex;
        }
        return bill;
    }

    private String mapToJson(Bill bill, LambdaLogger logger) {
        ObjectMapper jsonObjectMapper = new ObjectMapper();
        try {
            return jsonObjectMapper.writeValueAsString(bill);
        } catch (JsonProcessingException ex) {
            logger.log("Unable to map bill to Json");
            throw new JsonMappingException(ex);
        }
    }
}