package com.qanbio.training.aws.edrive;

public class PropertiesFileNotFoundException extends RuntimeException {
    public PropertiesFileNotFoundException(Throwable cause) {
        super(cause);
    }

    public PropertiesFileNotFoundException() {
    }
}
