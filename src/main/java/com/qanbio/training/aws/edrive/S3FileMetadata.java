package com.qanbio.training.aws.edrive;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class S3FileMetadata {
    private long contentLengh;
    private String contentType;
    private String bucketName;
    private String fileName;
}
