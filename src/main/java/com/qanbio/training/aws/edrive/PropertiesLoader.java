package com.qanbio.training.aws.edrive;

import lombok.SneakyThrows;

import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {
    private final static String PROPERTIES_FILE_NAME = "application.properties";

    private Properties properties;

    private PropertiesLoader() {
        properties = load();
    }

    public static PropertiesLoader getInstance() {
        return Holder.INSTANCE;
    }

    @SneakyThrows
    private static Properties load() {
        InputStream propertiesIs = PropertiesLoader.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME);

        if (propertiesIs == null) {
            throw new PropertiesFileNotFoundException();
        }

        Properties properties = new Properties();
        properties.load(propertiesIs);
        return properties;
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    private static class Holder {
        public static PropertiesLoader INSTANCE = new PropertiesLoader();
    }
}
