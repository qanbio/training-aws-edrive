package com.qanbio.training.aws.edrive;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class ExportFileRequestHandler implements RequestHandler<String, Bill>, AwsConfig {

    @Override
    public Bill handleRequest(String billId, Context context) {
        LambdaLogger logger = context.getLogger();

        //=== Build DynamoDB Client
        AmazonDynamoDB dbClient = buildDynamoDbClient();
        DynamoDBMapper dbMapper = new DynamoDBMapper(dbClient);

        //=== Return Object into Dynamo and S3
        return dbMapper.load(Bill.class, billId);
    }
}
