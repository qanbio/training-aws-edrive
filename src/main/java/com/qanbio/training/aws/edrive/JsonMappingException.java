package com.qanbio.training.aws.edrive;

public class JsonMappingException extends RuntimeException {
    public JsonMappingException(Throwable cause) {
        super(cause);
    }
}
