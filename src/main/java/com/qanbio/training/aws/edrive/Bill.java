package com.qanbio.training.aws.edrive;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBTable(tableName = "bills")
public class Bill {
    @DynamoDBHashKey(attributeName = "billId")
    private String billId;
    @DynamoDBAttribute(attributeName = "customerId")
    private String customerId;
    @DynamoDBAttribute(attributeName = "description")
    private String Description;
    @DynamoDBAttribute(attributeName = "amount")
    private String amount;

}